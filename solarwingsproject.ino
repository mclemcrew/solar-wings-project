/**********************************************************
 * SOLAR WINGS SENIOR DESIGN PROJECT ARUDINO CODE
 * WRITTEN BY: MICHAEL CLEMENS
 * LAST MODIFIED: 4/20/2017
 **********************************************************/
#include <Wire.h>
#include "RTClib.h"
#define NUM_SAMPLES 10
/*********************************************************
 * INSTANTIATE VARIABLES THAT WILL BE NECESSARY FOR THE REST OF THE PROJECT
 *********************************************************/
RTC_DS3231 rtc;
char data = 0; 
char newData = 0;
int j = 1;
char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
int valEncoderWest = 0;
int valEncoderEast = 0;          
int sec = 0;
int rtcYear,rtcMonth,rtcDay,rtcHour,rtcMin,rtcSec = 0;
int analogPinWest = 3;
int analogPinEast = 2;
int pushButtonWest = 11;
int pushButtonEast = 9;
int angleWest = 0;
int angleEast = 0;
int oldAngleWest = 0;
int oldAngleEast = 0;
int countForAngle = 0;
int countEast = 0;
int countWest = 0;
int intCount = 0;

void setup () {
  Serial.begin(9600);
  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC. Can't get current time!");
    while (1);
  }
  pinMode(analogPinWest, INPUT);                    // Encoder1 connection
  pinMode(analogPinEast, INPUT);                    // Encoder2 connection
  pinMode(13, OUTPUT);                              // Used to rotate West Motor
  pinMode(3, OUTPUT);                               // Used to control direction of rotation for West Motor
  pinMode(12, OUTPUT);                              // Used to rotate East Motor
  pinMode(4, OUTPUT);                               // Used to control direction of rotation for East Motor
  pinMode(10, OUTPUT);
  pinMode(7, INPUT);                                // Used to test the state of Bluetooth Module i.e. connected or not connected
  pinMode(8, OUTPUT);
  pinMode(pushButtonWest, INPUT);                   // Push Button for West Motor
  pinMode(pushButtonEast, INPUT);                   // Push Button for East Motor
}

void loop () {
    DateTime now = rtc.now();                       // Get the current RTC value
    valEncoderWest = analogRead(analogPinWest);     // read the input pin for Encoder 1
    valEncoderEast = analogRead(analogPinEast);     // read the input pin for Encoder 2
    int i=1;
    if(j==1) Serial.begin(9600);                    // Begin communcation with the RTC
    j=2;                                            // Once the communication begins we can then set the j to a different value so that the Serial communcation just keeps running and doesn't restart
    while (digitalRead(7)==1)                       // While a Bluetooth device is connected to the HC-05
    {
      j=1;
      if(i==1) Serial.begin(115200);        // Set different baud communication for Bluetooth device
      if(Serial.available() > 0)            // If there is data available to receive, we can send data dependent upon what we recieve
      {
          data = Serial.read();             // Read the incoming data & store into data
          Serial.print(data);               // Print Value inside data in Serial monitor
          Serial.print("\n");               // Print new line for clarity sake
          if(data == '1') {                 // Calibrate Button has been pressed
              calibrateWings();
              Serial.print(8,DEC);
              Serial.print("\n");
              Serial.flush();
          }
          else if(data == '2' ) {           // Stop Button has been pressed
              measureWings();
          }                             
          else if(data == '3' ) {           // Open Wings Button has been pressed
              openWings();
              Serial.print(9,DEC);
              Serial.print("\n");
              Serial.flush();
          }
          else if(data == '4' ) {           // Stop Button has been pressed
              stopWings();
          }
          else if(data == '5' ) {           // Resume Wings Button has been pressed
              resumeWings();
          }
          i=2;                              // This resets the Serial.begin to the baud rate of 9600 when communication with the Bluetooth module has been completed
      }
    }
  //Serial.print(now.hour());
    setSolarWings();
    //findSolarWingAngles();
    /*
    Serial.print('\n');
    Serial.print("The West Wing Angle is: ");
    Serial.print(angleWest);
    Serial.print('\n');
    Serial.print("The East Wing Angle is: ");
    Serial.print(angleEast);
    Serial.print('\n');
  */
    delay(1000);
    
}


void openWings() {
  setDirectionWestMotor(0);                     // Set direction of West Wing to move away from the solar panel
  setDirectionEastMotor(0);                     // Set direction of East Wing to move away from the solar panel
  while(digitalRead(pushButtonWest)!=LOW) {     // While the pushbutton has not been pressed, keep rotating the West Wing
    rotateWestMotor();
  }
  angleWest = 0;
  oldAngleWest = 0;
  while(digitalRead(pushButtonEast)!=LOW) {     // While the pushbutton has not been pressed, keep rotating the East Wing
    rotateEastMotor();
  }
  angleEast = 0;
  oldAngleEast = 0;
}

void calibrateWings() {
  openWings();
  setSolarWings();
}

void rotateWestMotor() {                      // Rotate West Wing Motor at 20kHz (max speed for the motor)
    digitalWrite(13, HIGH);
    delayMicroseconds(40);
    digitalWrite(13, LOW);
    delayMicroseconds(10);
}

void rotateEastMotor() {
    digitalWrite(12, HIGH);                   // Rotate East Wing Motor at 20kHz (max speed for the motor)
    delayMicroseconds(40); 
    digitalWrite(12, LOW);
    delayMicroseconds(10);
}

void rotateWestMotorCalibrate() {                      // Rotate West Wing Motor at 20kHz (max speed for the motor)
    digitalWrite(13, HIGH);
    delayMicroseconds(80);
    digitalWrite(13, LOW);
    delayMicroseconds(1);
}

void rotateEastMotorCalibrate() {
    digitalWrite(12, HIGH);                   // Rotate East Wing Motor at 20kHz (max speed for the motor)
    delayMicroseconds(80); 
    digitalWrite(12, LOW);
    delayMicroseconds(1);
}

void stopWings() {
   //STOP OPERATION UNTIL USER PRESSES THE RESUME BUTTON
   while(newData!='5') {
      newData = Serial.read();           // Read the incoming data & store into data
   }
   Serial.print(newData);
   Serial.print('\n');
   
   newData = '0';
   return;
}

void resumeWings() {
  setSolarWings();
}

void setDirectionWestMotor(int x) {   
  if(x==1) {                        // 1 will rotate the wing towards the solar panel
    digitalWrite(3, HIGH);
  }
  else if(x==0) {                   // 0 will rotate the wing away from the solar panel
    digitalWrite(3, LOW);
  }
}

void setDirectionEastMotor(int x) {
  if(x==1) {                        // 1 will rotate the wing towards the solar panel
    digitalWrite(4, HIGH);
  }
  else if(x==0) {                   // 0 will rotate the wing away from the solar panel
    digitalWrite(4, LOW);
  }
}

void measureWings() {
  int sum = 0;                    // sum of samples taken
  unsigned char sample_count = 0; // current sample number
  float voltage = 0.0;            // calculated voltage
  // take a number of analog samples and add them up
    while (sample_count < NUM_SAMPLES) {
        sum += analogRead(A0);
        sample_count++;
        delay(10);
    }
    // calculate the voltage
    // use 5.0 for a 5.0V ADC reference voltage
    // 5.015V is the calibrated reference voltage
    voltage = ((float)sum / (float)NUM_SAMPLES * 5.015) / 1024.0;
    // send voltage for display on Serial Monitor
    voltage = voltage * 11.132;
    // voltage multiplied by 11 when using voltage divider that
    // divides by 11. 11.132 is the calibrated voltage divide
    // value
    int sendVoltage = 0;
    int valueVoltage[4];
    if(voltage>=10) {
      sendVoltage = voltage * 100;
      for(int i=0;i<=3;i++) {
         valueVoltage[i] = sendVoltage % 10;
         sendVoltage /= 10;
      }
      Serial.write(1);
    }
    else {
      sendVoltage = voltage * 1000;
      for(int i=0;i<=3;i++) {
         valueVoltage[i] = sendVoltage % 10;
         sendVoltage /= 10;
      }
      Serial.write(2);  
    }
    Serial.write(valueVoltage[0]);
    Serial.write(valueVoltage[1]);
    Serial.write(valueVoltage[2]);
    Serial.write(valueVoltage[3]);

    //Fix this please
    Serial.write(2); 
    Serial.write(valueVoltage[0]);
    Serial.write(valueVoltage[1]);
    Serial.write(valueVoltage[2]);
    Serial.write(valueVoltage[3]);
}

void setSolarWings() {
    findSolarWingAngles();
    /*
    Serial.print("\n");
    Serial.print(oldAngleWest);
    Serial.print("\n");
    Serial.print(oldAngleEast);
    Serial.print("\n");
    Serial.print(angleWest);
    Serial.print("\n");
    Serial.print(angleEast);
    */
    countWest = 0;
    countEast = 0;
    countForAngle = 0;
    intCount = 0;
    valEncoderWest = analogRead(analogPinWest);
    valEncoderEast = analogRead(analogPinEast); 
    if(oldAngleWest!=angleWest) {
      if(oldAngleWest>angleWest) {
        setDirectionWestMotor(0);
        countForAngle = oldAngleWest - angleWest;
      }
      else {
        setDirectionWestMotor(1);
        countForAngle = angleWest - oldAngleWest;
      }
        while(countWest!=countForAngle) {
          /*
          if(countWest==8||countWest==18||countWest==28) {}
          else{
          Serial.print(countWest);
          Serial.print('\n');
          }
          Serial.print(countForAngle);
          Serial.print('\n');
          */
          if(valEncoderWest>=500) {
            while(intCount!=5) {
              valEncoderWest = analogRead(analogPinWest);
              rotateWestMotorCalibrate();
              if(valEncoderWest<500) {
                intCount = intCount + 1;
              }
            }
            countWest = countWest + 1;
            intCount = 0;
            continue;
          }
          else {
            while(intCount!=5) {
              valEncoderWest = analogRead(analogPinWest);
              rotateWestMotorCalibrate();
              if(valEncoderWest>=500) {
                intCount = intCount + 1;
              }
            }
            countWest = countWest + 1;
            intCount = 0;
            continue;
          }
        }
      }
   
    if(oldAngleEast!=angleEast) {
      if(oldAngleEast>angleEast) {
        setDirectionEastMotor(0);
        countForAngle = oldAngleEast - angleEast;
      }
      else {
        setDirectionEastMotor(1);
        countForAngle = angleEast - oldAngleEast;
      }
        while(countEast!=countForAngle) {
          /*
          if(countEast==8||countEast==18||countEast==28) {}
          else{
          Serial.print(countEast);
          Serial.print('\n');
          }
          Serial.print(countForAngle);
          Serial.print('\n');
          */
          if(valEncoderEast>=500) {
            while(intCount!=5) {
              valEncoderEast = analogRead(analogPinEast); 
              rotateEastMotorCalibrate();
              if(valEncoderEast<500) {
                intCount = intCount + 1;
              }
            }
            countEast = countEast + 1;
            intCount = 0;
            continue;
          }
          else {
            while(intCount!=5) {
              valEncoderEast = analogRead(analogPinEast); 
              rotateEastMotorCalibrate();
              if(valEncoderEast>=500) {
                intCount = intCount + 1;
              }
            }
            countEast = countEast + 1;
            intCount = 0;
            continue;
          }
        }
      }
    oldAngleWest = angleWest;
    oldAngleEast = angleEast;
    return;  
}

void findSolarWingAngles() {
  //Finds the approximate angles of both the West Wing and East Wing corresponding to the approxmiate 
  DateTime now = rtc.now();
  rtcYear = now.year();
  rtcMonth = now.month();
  rtcDay = now.day();
  rtcHour = now.hour();
  rtcMin = now.minute();
  rtcSec = now.second();
  if(rtcHour==5&&rtcMin>=50) {
       angleWest = 120;
       angleEast = 0;
    return;
  }
  /***********************************************************************
   * THIS IS THE 1ST SET OF FOUR ROTATIONS
   ***********************************************************************/
  if(rtcHour==6&&rtcMin<=8) {
    if(rtcMin==8&&rtcSec<15) {
       angleWest = 120;
       angleEast = 0;
    }
       angleWest = 120;
       angleEast = 0;
  }
  if(rtcHour==6&&rtcMin>=8&&rtcMin<=16) {
    if(rtcMin==8&&rtcSec>=15){
       angleWest = 119;
       angleEast = 0;
      return;
    }
    else if(rtcMin==16&&rtcSec<30) {
       angleWest = 119;
       angleEast = 0;
      return;
    }
       angleWest = 119;
       angleEast = 0;
  }
  if(rtcHour==6&&rtcMin>=16&&rtcMin<=24) {
    if(rtcMin==16&&rtcSec>=30){
       angleWest = 118;
       angleEast = 0;
      return;
    }
    else if(rtcMin==24&&rtcSec<45) {
       angleWest = 118;
       angleEast = 0;
      return;
    }
       angleWest = 118;
       angleEast = 0;
  }
  if(rtcHour==6&&rtcMin>=24&&rtcMin<=32) {
    if(rtcMin==24&&rtcSec>=45){
       angleWest = 117;
       angleEast = 0;
      return;
    }
    else if(rtcMin==32&&rtcSec<=59) {
       angleWest = 117;
       angleEast = 0;
      return;
    }
       angleWest = 117;
       angleEast = 0;
  }
  /***********************************************************************
   * THIS IS THE 2ND SET OF FOUR ROTATIONS
   ***********************************************************************/
  if(rtcHour==6&&rtcMin>=33&&rtcMin<=41) {
    if(rtcMin==41&&rtcSec<15) {
       angleWest = 116;
       angleEast = 0;
       return;
    }
       angleWest = 116;
       angleEast = 0;
  }
  if(rtcHour==6&&rtcMin>=41&&rtcMin<=49) {
    if(rtcMin==41&&rtcSec>=15){
       angleWest = 115;
       angleEast = 0;
      return;
    }
    else if(rtcMin==49&&rtcSec<30) {
       angleWest = 115;
       angleEast = 0;
      return;
    }
       angleWest = 115;
       angleEast = 0;
  }
  if(rtcHour==6&&rtcMin>=49&&rtcMin<=57) {
    if(rtcMin==16&&rtcSec>=30){
       angleWest = 114;
       angleEast = 0;
      return;
    }
    else if(rtcMin==57&&rtcSec<45) {
       angleWest = 114;
       angleEast = 0;
      return;
    }
       angleWest = 114;
       angleEast = 0;
  }
  if((rtcHour==6&&rtcMin>=57)||(rtcHour==7&&rtcMin<=5)) {
    if(rtcMin==57&&rtcSec>=45){
       angleWest = 113;
       angleEast = 0;
      return;
    }
    else if(rtcMin==5&&rtcSec<=59) {
       angleWest = 113;
       angleEast = 0;
      return;
    }
       angleWest = 113;
       angleEast = 0; 
  }
  /***********************************************************************
   * THIS IS THE 3RD SET OF FOUR ROTATIONS
   ***********************************************************************/
  if(rtcHour==7&&rtcMin>=6&&rtcMin<=14) {
    if(rtcMin==14&&rtcSec<15) {
       angleWest = 112;
       angleEast = 0;
       return;
    }
       angleWest = 112;
       angleEast = 0;
  }
  if(rtcHour==7&&rtcMin>=14&&rtcMin<=22) {
    if(rtcMin==14&&rtcSec>=15){
       angleWest = 111;
       angleEast = 0;
      return;
    }
    else if(rtcMin==22&&rtcSec<30) {
       angleWest = 111;
       angleEast = 0;
      return;
    }
       angleWest = 111;
       angleEast = 0;
  }
  if(rtcHour==7&&rtcMin>=22&&rtcMin<=30) {
    if(rtcMin==22&&rtcSec>=30){
       angleWest = 110;
       angleEast = 0;
      return;
    }
    else if(rtcMin==30&&rtcSec<45) {
       angleWest = 110;
       angleEast = 0;
      return;
    }
       angleWest = 110;
       angleEast = 0;
  }
  if(rtcHour==7&&rtcMin>=30&&rtcMin<=38) {
    if(rtcMin==30&&rtcSec>=45){
       angleWest = 109;
       angleEast = 0;
      return;
    }
    else if(rtcMin==38&&rtcSec<=59) {
       angleWest = 109;
       angleEast = 0;
      return;
    }
       angleWest = 109;
       angleEast = 0; 
  }
  /***********************************************************************
   * THIS IS THE 4TH SET OF FOUR ROTATIONS
   ***********************************************************************/
  if(rtcHour==7&&rtcMin>=39&&rtcMin<=47) {
    if(rtcMin==47&&rtcSec<15) {
       angleWest = 108;
       angleEast = 0;
       return;
    }
       angleWest = 108;
       angleEast = 0;
  }
  if(rtcHour==7&&rtcMin>=47&&rtcMin<=55) {
    if(rtcMin==47&&rtcSec>=15){
       angleWest = 107;
       angleEast = 0;
      return;
    }
    else if(rtcMin==55&&rtcSec<30) {
       angleWest = 107;
       angleEast = 0;
      return;
    }
       angleWest = 107;
       angleEast = 0;
  }
  if((rtcHour==7&&rtcMin>=55)||(rtcHour==8&&rtcMin<=3)) {
    if(rtcMin==55&&rtcSec>=30){
       angleWest = 106;
       angleEast = 0;
      return;
    }
    else if(rtcMin==8&&rtcSec<45) {
       angleWest = 106;
       angleEast = 0;
      return;
    }
       angleWest = 106;
       angleEast = 0;
  }
  if(rtcHour==8&&rtcMin>=3&&rtcMin<=11) {
    if(rtcMin==3&&rtcSec>=45){
       angleWest = 105;
       angleEast = 0;
      return;
    }
    else if(rtcMin==11&&rtcSec<=59) {
       angleWest = 105;
       angleEast = 0;
      return;
    }
       angleWest = 105;
       angleEast = 0; 
  }
  /***********************************************************************
   * THIS IS THE 5TH SET OF FOUR ROTATIONS
   ***********************************************************************/
  if(rtcHour==8&&rtcMin>=12&&rtcMin<=20) {
    if(rtcMin==20&&rtcSec<15) {
       angleWest = 104;
       angleEast = 0;
       return;
    }
       angleWest = 104;
       angleEast = 0;
  }
  if(rtcHour==8&&rtcMin>=20&&rtcMin<=28) {
    if(rtcMin==20&&rtcSec>=15){
       angleWest = 104;
       angleEast = 0;
      return;
    }
    else if(rtcMin==28&&rtcSec<30) {
       angleWest = 103;
       angleEast = 0;
      return;
    }
       angleWest = 103;
       angleEast = 0;
  }
  if(rtcHour==8&&rtcMin>=28&&rtcMin<=36) {
    if(rtcMin==28&&rtcSec>=30){
       angleWest = 102;
       angleEast = 0;
      return;
    }
    else if(rtcMin==36&&rtcSec<45) {
       angleWest = 102;
       angleEast = 0;
      return;
    }
       angleWest = 102;
       angleEast = 0;
  }
  if(rtcHour==8&&rtcMin>=36&&rtcMin<=48) {
    if(rtcMin==36&&rtcSec>=45){
       angleWest = 101;
       angleEast = 0;
      return;
    }
    else if(rtcMin==48&&rtcSec<=59) {
       angleWest = 101;
       angleEast = 0;
      return;
    }
       angleWest = 101;
       angleEast = 0; 
  }
/***********************************************************************
   * THIS IS THE 6TH SET OF FOUR ROTATIONS
   ***********************************************************************/
  if(rtcHour==8&&rtcMin>=48&&rtcMin<=56) {
    if(rtcMin==56&&rtcSec<15) {
       angleWest = 100;
       angleEast = 0;
       return;
    }
       angleWest = 100;
       angleEast = 0;
  }
  if((rtcHour==8&&rtcMin>=56)||(rtcHour==9&&rtcMin<=4)) {
    if(rtcMin==56&&rtcSec>=15){
       angleWest = 99;
       angleEast = 0;
      return;
    }
    else if(rtcMin==4&&rtcSec<30) {
       angleWest = 99;
       angleEast = 0;
      return;
    }
        angleWest = 99;
       angleEast = 0;
  }
  if(rtcHour==9&&rtcMin>=4&&rtcMin<=12) {
    if(rtcMin==4&&rtcSec>=30){
       angleWest = 98;
       angleEast = 0;
      return;
    }
    else if(rtcMin==12&&rtcSec<45) {
       angleWest = 98;
       angleEast = 0;
      return;
    }
       angleWest = 98;
       angleEast = 0;
  }
  if(rtcHour==9&&rtcMin>=4&&rtcMin<=12) {
    if(rtcMin==4&&rtcSec>=45){
       angleWest = 97;
       angleEast = 0;
      return;
    }
    else if(rtcMin==12&&rtcSec<=59) {
       angleWest = 97;
       angleEast = 0;
      return;
    }
       angleWest = 97;
       angleEast = 0; 
  }
/***********************************************************************
   * THIS IS THE 7TH SET OF FOUR ROTATIONS
   ***********************************************************************/
  if(rtcHour==9&&rtcMin>=13&&rtcMin<=21) {
    if(rtcMin==21&&rtcSec<15) {
       angleWest = 96;
       angleEast = 0;
       return;
    }
       angleWest = 96;
       angleEast = 0;
  }
  if(rtcHour==9&&rtcMin>=21&&rtcMin<=29){
    if(rtcMin==21&&rtcSec>=15){
       angleWest = 95;
       angleEast = 0;
      return;
    }
    else if(rtcMin==29&&rtcSec<30) {
       angleWest = 95;
       angleEast = 0;
      return;
    }
       angleWest = 95;
       angleEast = 0;
  }
  if(rtcHour==9&&rtcMin>=29&&rtcMin<=37) {
    if(rtcMin==29&&rtcSec>=30){
       angleWest = 94;
       angleEast = 0;
      return;
    }
    else if(rtcMin==37&&rtcSec<45) {
       angleWest = 94;
       angleEast = 0;
      return;
    }
       angleWest = 94;
       angleEast = 0;
  }
  if(rtcHour==9&&rtcMin>=37&&rtcMin<=45) {
    if(rtcMin==37&&rtcSec>=45){
       angleWest = 93;
       angleEast = 0;
      return;
    }
    else if(rtcMin==45&&rtcSec<=59) {
       angleWest = 93;
       angleEast = 0;
      return;
    }
       angleWest = 93;
       angleEast = 0;
  }
/***********************************************************************
   * THIS IS THE 8TH SET OF FOUR ROTATIONS
   ***********************************************************************/
  if(rtcHour==9&&rtcMin>=46&&rtcMin<=54) {
    if(rtcMin==54&&rtcSec<15) {
       angleWest = 92;
       angleEast = 0;
       return;
    }
       angleWest = 92;
       angleEast = 0;
  }
  if((rtcHour==9&&rtcMin>=54)||(rtcHour==10&&rtcMin<=2)) {
    if(rtcMin==54&&rtcSec>=15){
       angleWest = 91;
       angleEast = 0;
      return;
    }
    else if(rtcMin==2&&rtcSec<30) {
       angleWest = 91;
       angleEast = 0;
      return;
    }
       angleWest = 91;
       angleEast = 0;
  }
  if(rtcHour==10&&rtcMin>=2&&rtcMin<=10) {
    if(rtcMin==2&&rtcSec>=30){
       angleWest = 90;
       angleEast = 0;
      return;
    }
    else if(rtcMin==10&&rtcSec<45) {
       angleWest = 90;
       angleEast = 0;
      return;
    }
        angleWest = 90;
       angleEast = 0;
  }
  if(rtcHour==10&&rtcMin>=10&&rtcMin<=18) {
    if(rtcMin==10&&rtcSec>=45){
       angleWest = 89;
       angleEast = 0;
      return;
    }
    else if(rtcMin==18&&rtcSec<=59) {
       angleWest = 89;
       angleEast = 0;
      return;
    }
       angleWest = 89;
       angleEast = 0; 
  }
/***********************************************************************
   * THIS IS THE 9TH SET OF FOUR ROTATIONS
   ***********************************************************************/
  if(rtcHour==10&&rtcMin>=19&&rtcMin<=27) {
    if(rtcMin==27&&rtcSec<15) {
       angleWest = 88;
       angleEast = 0;
       return;
    }
       angleWest = 88;
       angleEast = 0;
  }
  if(rtcHour==10&&rtcMin>=27&&rtcMin<=36){
    if(rtcMin==27&&rtcSec>=15){
       angleWest = 87;
       angleEast = 0;
      return;
    }
    else if(rtcMin==36&&rtcSec<30) {
       angleWest = 87;
       angleEast = 0;
      return;
    }
       angleWest = 87;
       angleEast = 0;
  }
  if(rtcHour==10&&rtcMin>=36&&rtcMin<=48) {
    if(rtcMin==36&&rtcSec>=30){
       angleWest = 86;
       angleEast = 0;
      return;
    }
    else if(rtcMin==48&&rtcSec<45) {
       angleWest = 86;
       angleEast = 0;
      return;
    }
       angleWest = 86;
       angleEast = 0;
  }
  if(rtcHour==10&&rtcMin>=48&&rtcMin<=56) {
    if(rtcMin==48&&rtcSec>=45){
       angleWest = 85;
       angleEast = 0;
      return;
    }
    else if(rtcMin==56&&rtcSec<=59) {
       angleWest = 85;
       angleEast = 0;
      return;
    }
       angleWest = 85;
       angleEast = 0;
  }
/***********************************************************************
   * THIS IS THE 10TH SET OF FOUR ROTATIONS
   ***********************************************************************/
  if((rtcHour==10&&rtcMin>=57)||(rtcHour==11&&rtcMin<=5)) {
    if(rtcMin==5&&rtcSec<15) {
       angleWest = 84;
       angleEast = 0;
       return;
    }
       angleWest = 84;
       angleEast = 0;
  }
  if(rtcHour==11&&rtcMin>=5&&rtcMin<=13) {
    if(rtcMin==5&&rtcSec>=15){
       angleWest = 83;
       angleEast = 0;
      return;
    }
    else if(rtcMin==13&&rtcSec<30) {
       angleWest = 83;
       angleEast = 0;
      return;
    }
       angleWest = 83;
       angleEast = 0;
  }
  if(rtcHour==11&&rtcMin>=13&&rtcMin<=21) {
    if(rtcMin==13&&rtcSec>=30){
       angleWest = 82;
       angleEast = 0;
      return;
    }
    else if(rtcMin==21&&rtcSec<45) {
       angleWest = 82;
       angleEast = 0;
      return;
    }
       angleWest = 82;
       angleEast = 0;
  }
  if(rtcHour==11&&rtcMin>=21&&rtcMin<=29) {
    if(rtcMin==21&&rtcSec>=45){
       angleWest = 81;
       angleEast = 0;
      return;
    }
    else if(rtcMin==29&&rtcSec<=59) {
       angleWest = 81;
       angleEast = 0;
      return;
    }
       angleWest = 81;
       angleEast = 0; 
  }
 /***********************************************************************
   * WE HAVE ONE FINAL 1DEGREE ROTATITION OF THE WEST WING
   ***********************************************************************/ 
  if(rtcHour==11&&rtcMin>=30&&rtcMin<=39) {
       angleWest = 80;
       angleEast = 0;
       return; 
  }
 /***********************************************************************
   * LAY THE PANELS FLAT BECAUSE THE SUN IS DIRECTLY OVERHEAD
   ***********************************************************************/ 
  if((rtcHour==11&&rtcMin>=40)||(rtcHour==12&&rtcMin<=19)) {
       angleWest = 0;
       angleEast = 0;
       return;  
  }
 /***********************************************************************
   * NOW WE START MOVING THE EAST WING IS THE OPPOSITE FASHION OF THE WEST WING
   ***********************************************************************/ 
 
  /***********************************************************************
   * THIS IS THE 1ST SET OF FOUR ROTATIONS FOR EW
   ***********************************************************************/
  if(rtcHour==12&&rtcMin>=20&&rtcMin<=38) {
    if(rtcMin==38&&rtcSec<15) {
       angleWest = 0;
       angleEast = 80;
       return;
    }
       angleWest = 0;
       angleEast = 80;
  }
  if(rtcHour==12&&rtcMin>=38&&rtcMin<=46) {
    if(rtcMin==38&&rtcSec>=15){
       angleWest = 0;
       angleEast = 81;
      return;
    }
    else if(rtcMin==46&&rtcSec<30) {
       angleWest = 0;
       angleEast = 81;
      return;
    }
       angleWest = 0;
       angleEast = 81;
  }
  if(rtcHour==12&&rtcMin>=46&&rtcMin<=54) {
    if(rtcMin==46&&rtcSec>=30){
       angleWest = 0;
       angleEast = 82;
      return;
    }
    else if(rtcMin==54&&rtcSec<45) {
       angleWest = 0;
       angleEast = 82;
      return;
    }
       angleWest = 0;
       angleEast = 82;
  }
  if((rtcHour==12&&rtcMin>=54)||(rtcHour==13&&rtcMin<=2)) {
    if(rtcMin==54&&rtcSec>=45){
       angleWest = 0;
       angleEast = 83;
      return;
    }
    else if(rtcMin==2&&rtcSec<=59) {
       angleWest = 0;
       angleEast = 83;
      return;
    }
      angleWest = 0;
      angleEast = 83;
  }
  /***********************************************************************
   * THIS IS THE 2ND SET OF FOUR ROTATIONS FOR EW
   ***********************************************************************/
  if(rtcHour==13&&rtcMin>=3&&rtcMin<=11) {
    if(rtcMin==11&&rtcSec<15) {
       angleWest = 0;
       angleEast = 84;
       return;
    }
       angleWest = 0;
       angleEast = 84;
  }
  if(rtcHour==13&&rtcMin>=11&&rtcMin<=19) {
    if(rtcMin==11&&rtcSec>=15){
       angleWest = 0;
       angleEast = 85;
      return;
    }
    else if(rtcMin==19&&rtcSec<30) {
       angleWest = 0;
       angleEast = 85;
      return;
    }
       angleWest = 0;
       angleEast = 85;
  }
  if(rtcHour==13&&rtcMin>=19&&rtcMin<=27) {
    if(rtcMin==19&&rtcSec>=30){
       angleWest = 0;
       angleEast = 86;
      return;
    }
    else if(rtcMin==27&&rtcSec<45) {
       angleWest = 0;
       angleEast = 86;
      return;
    }
       angleWest = 0;
       angleEast = 86;
  }
  if(rtcHour==13&&rtcMin>=27&&rtcMin<=35) {
    if(rtcMin==27&&rtcSec>=45){
       angleWest = 0;
       angleEast = 87;
      return;
    }
    else if(rtcMin==35&&rtcSec<=59) {
       angleWest = 0;
       angleEast = 87;
      return;
    }
      angleWest = 0;
      angleEast = 87;
  }
/***********************************************************************
   * THIS IS THE 3RD SET OF FOUR ROTATIONS FOR EW
   ***********************************************************************/
  if(rtcHour==13&&rtcMin>=36&&rtcMin<=44) {
    if(rtcMin==44&&rtcSec<15) {
       angleWest = 0;
       angleEast = 88;
       return;
    }
       angleWest = 0;
       angleEast = 88;
  }
  if(rtcHour==13&&rtcMin>=44&&rtcMin<=52) {
    if(rtcMin==44&&rtcSec>=15){
       angleWest = 0;
       angleEast = 89;
      return;
    }
    else if(rtcMin==52&&rtcSec<30) {
       angleWest = 0;
       angleEast = 89;
      return;
    }
       angleWest = 0;
       angleEast = 89;
  }
  if((rtcHour==13&&rtcMin>=52)||(rtcHour==14&&rtcMin==0)) {
    if(rtcMin==52&&rtcSec>=30){
       angleWest = 0;
       angleEast = 90;
      return;
    }
    else if(rtcMin==0&&rtcSec<45) {
       angleWest = 0;
       angleEast = 90;
      return;
    }
       angleWest = 0;
       angleEast = 90;
  }
  if(rtcHour==14&&rtcMin>=0&&rtcMin<=8) {
    if(rtcMin==0&&rtcSec>=45){
       angleWest = 0;
       angleEast = 91;
      return;
    }
    else if(rtcMin==8&&rtcSec<=59) {
       angleWest = 0;
       angleEast = 91;
      return;
    }
      angleWest = 0;
      angleEast = 91;
  }
  /***********************************************************************
   * THIS IS THE 4TH SET OF FOUR ROTATIONS FOR EW
   ***********************************************************************/
  if(rtcHour==14&&rtcMin>=9&&rtcMin<=17) {
    if(rtcMin==17&&rtcSec<15) {
       angleWest = 0;
       angleEast = 92;
       return;
    }
       angleWest = 0;
       angleEast = 92;
  }
  if(rtcHour==14&&rtcMin>=17&&rtcMin<=25) {
    if(rtcMin==17&&rtcSec>=15){
       angleWest = 0;
       angleEast = 93;
      return;
    }
    else if(rtcMin==25&&rtcSec<30) {
       angleWest = 0;
       angleEast = 93;
      return;
    }
       angleWest = 0;
       angleEast = 93;
  }
  if(rtcHour==14&&rtcMin>=25&&rtcMin<=33) {
    if(rtcMin==25&&rtcSec>=30){
       angleWest = 0;
       angleEast = 94;
      return;
    }
    else if(rtcMin==33&&rtcSec<45) {
       angleWest = 0;
       angleEast = 94;
      return;
    }
       angleWest = 0;
       angleEast = 94;
  }
  if(rtcHour==14&&rtcMin>=33&&rtcMin<=41) {
    if(rtcMin==33&&rtcSec>=45){
       angleWest = 0;
       angleEast = 95;
      return;
    }
    else if(rtcMin==41&&rtcSec<=59) {
       angleWest = 0;
       angleEast = 95;
      return;
    }
      angleWest = 0;
      angleEast = 95;
  }
/***********************************************************************
   * THIS IS THE 5TH SET OF FOUR ROTATIONS FOR EW
   ***********************************************************************/
  if(rtcHour==14&&rtcMin>=42&&rtcMin<=50) {
    if(rtcMin==50&&rtcSec<15) {
       angleWest = 0;
       angleEast = 96;
       return;
    }
       angleWest = 0;
       angleEast = 96;
  }
  if(rtcHour==14&&rtcMin>=50&&rtcMin<=58) {
    if(rtcMin==50&&rtcSec>=15){
       angleWest = 0;
       angleEast = 97;
      return;
    }
    else if(rtcMin==58&&rtcSec<30) {
       angleWest = 0;
       angleEast = 97;
      return;
    }
       angleWest = 0;
       angleEast = 97;
  }
  if((rtcHour==14&&rtcMin>=58)||(rtcHour==15&&rtcMin==6)) {
    if(rtcMin==58&&rtcSec>=30){
       angleWest = 0;
       angleEast = 98;
      return;
    }
    else if(rtcMin==6&&rtcSec<45) {
       angleWest = 0;
       angleEast = 98;
      return;
    }
       angleWest = 0;
       angleEast = 98;
  }
  if(rtcHour==15&&rtcMin>=6&&rtcMin<=14) {
    if(rtcMin==6&&rtcSec>=45){
       angleWest = 0;
       angleEast = 99;
      return;
    }
    else if(rtcMin==14&&rtcSec<=59) {
       angleWest = 0;
       angleEast = 99;
      return;
    }
      angleWest = 0;
      angleEast = 99;
  }
  /***********************************************************************
   * THIS IS THE 6TH SET OF FOUR ROTATIONS FOR EW
   ***********************************************************************/
  if(rtcHour==15&&rtcMin>=15&&rtcMin<=23) {
    if(rtcMin==23&&rtcSec<15) {
       angleWest = 0;
       angleEast = 100;
       return;
    }
       angleWest = 0;
       angleEast = 100;
  }
  if(rtcHour==15&&rtcMin>=23&&rtcMin<=31) {
    if(rtcMin==23&&rtcSec>=15){
       angleWest = 0;
       angleEast = 101;
      return;
    }
    else if(rtcMin==31&&rtcSec<30) {
       angleWest = 0;
       angleEast = 101;
      return;
    }
       angleWest = 0;
       angleEast = 101;
  }
  if(rtcHour==15&&rtcMin>=31&&rtcMin<=39) {
    if(rtcMin==31&&rtcSec>=30){
       angleWest = 0;
       angleEast = 102;
      return;
    }
    else if(rtcMin==39&&rtcSec<45) {
       angleWest = 0;
       angleEast = 102;
      return;
    }
       angleWest = 0;
       angleEast = 102;
  }
  if(rtcHour==15&&rtcMin>=39&&rtcMin<=47) {
    if(rtcMin==39&&rtcSec>=45){
       angleWest = 0;
       angleEast = 103;
      return;
    }
    else if(rtcMin==47&&rtcSec<=59) {
       angleWest = 0;
       angleEast = 103;
      return;
    }
      angleWest = 0;
      angleEast = 103;
  }
/***********************************************************************
   * THIS IS THE 7TH SET OF FOUR ROTATIONS FOR EW
   ***********************************************************************/
  if(rtcHour==15&&rtcMin>=48&&rtcMin<=56) {
    if(rtcMin==56&&rtcSec<15) {
       angleWest = 0;
       angleEast = 104;
       return;
    }
       angleWest = 0;
       angleEast = 104;
  }
  if((rtcHour==15&&rtcMin>=56)||(rtcHour==16&&rtcMin==4)) {
    if(rtcMin==56&&rtcSec>=15){
       angleWest = 0;
       angleEast = 105;
      return;
    }
    else if(rtcMin==4&&rtcSec<30) {
       angleWest = 0;
       angleEast = 105;
      return;
    }
       angleWest = 0;
       angleEast = 105;
  }
  if(rtcHour==16&&rtcMin>=4&&rtcMin<=12) {
    if(rtcMin==4&&rtcSec>=30){
       angleWest = 0;
       angleEast = 106;
      return;
    }
    else if(rtcMin==12&&rtcSec<45) {
       angleWest = 0;
       angleEast = 106;
      return;
    }
       angleWest = 0;
       angleEast = 106;
  }
  if(rtcHour==16&&rtcMin>=12&&rtcMin<=20) {
    if(rtcMin==12&&rtcSec>=45){
       angleWest = 0;
       angleEast = 107;
      return;
    }
    else if(rtcMin==20&&rtcSec<=59) {
       angleWest = 0;
       angleEast = 107;
      return;
    }
      angleWest = 0;
      angleEast = 107;
  }
  /***********************************************************************
   * THIS IS THE 8TH SET OF FOUR ROTATIONS FOR EW
   ***********************************************************************/
  if(rtcHour==16&rtcMin>=21&&rtcMin<=29) {
    if(rtcMin==29&&rtcSec<15) {
       angleWest = 0;
       angleEast = 108;
       return;
    }
       angleWest = 0;
       angleEast = 108;
  }
  if(rtcHour==16&&rtcMin>=29&&rtcMin<=37) {
    if(rtcMin==29&&rtcSec>=15){
       angleWest = 0;
       angleEast = 109;
      return;
    }
    else if(rtcMin==37&&rtcSec<30) {
       angleWest = 0;
       angleEast = 109;
      return;
    }
       angleWest = 0;
       angleEast = 109;
  }
  if(rtcHour==16&&rtcMin>=37&&rtcMin<=45) {
    if(rtcMin==37&&rtcSec>=30){
       angleWest = 0;
       angleEast = 110;
      return;
    }
    else if(rtcMin==45&&rtcSec<45) {
       angleWest = 0;
       angleEast = 110;
      return;
    }
       angleWest = 0;
       angleEast = 110;
  }
  if(rtcHour==16&&rtcMin>=45&&rtcMin<=53) {
    if(rtcMin==45&&rtcSec>=45){
       angleWest = 0;
       angleEast = 111;
      return;
    }
    else if(rtcMin==53&&rtcSec<=59) {
       angleWest = 0;
       angleEast = 111;
      return;
    }
      angleWest = 0;
      angleEast = 111;
  }
/***********************************************************************
   * THIS IS THE 9TH SET OF FOUR ROTATIONS FOR EW
   ***********************************************************************/
  if((rtcHour==16&&rtcMin>=54)||(rtcHour==17&&rtcMin==2)) {
    if(rtcMin==2&&rtcSec<15) {
       angleWest = 0;
       angleEast = 112;
       return;
    }
       angleWest = 0;
       angleEast = 112;
  }
  if(rtcHour==17&&rtcMin>=2&&rtcMin<=10) {
    if(rtcMin==2&&rtcSec>=15){
       angleWest = 0;
       angleEast = 113;
      return;
    }
    else if(rtcMin==10&&rtcSec<30) {
       angleWest = 0;
       angleEast = 113;
      return;
    }
       angleWest = 0;
       angleEast = 113;
  }
  if(rtcHour==17&&rtcMin>=10&&rtcMin<=18) {
    if(rtcMin==10&&rtcSec>=30){
       angleWest = 0;
       angleEast = 114;
      return;
    }
    else if(rtcMin==18&&rtcSec<45) {
       angleWest = 0;
       angleEast = 114;
      return;
    }
       angleWest = 0;
       angleEast = 114;
  }
  if(rtcHour==17&&rtcMin>=18&&rtcMin<=26) {
    if(rtcMin==18&&rtcSec>=45){
       angleWest = 0;
       angleEast = 115;
      return;
    }
    else if(rtcMin==26&&rtcSec<=59) {
       angleWest = 0;
       angleEast = 115;
      return;
    }
      angleWest = 0;
      angleEast = 115;
  }
/***********************************************************************
   * THIS IS THE 10TH SET OF FOUR ROTATIONS FOR EW
   ***********************************************************************/
  if(rtcHour==17&&rtcMin>=27&&rtcMin<=35) {
    if(rtcMin==35&&rtcSec<15) {
       angleWest = 0;
       angleEast = 116;
       return;
    }
       angleWest = 0;
       angleEast = 116;
  }
  if(rtcHour==17&&rtcMin>=35&&rtcMin<=43) {
    if(rtcMin==35&&rtcSec>=15){
       angleWest = 0;
       angleEast = 117;
      return;
    }
    else if(rtcMin==43&&rtcSec<30) {
       angleWest = 0;
       angleEast = 117;
      return;
    }
       angleWest = 0;
       angleEast = 117;
  }
  if(rtcHour==17&&rtcMin>=43&&rtcMin<=51) {
    if(rtcMin==43&&rtcSec>=30){
       angleWest = 0;
       angleEast = 118;
      return;
    }
    else if(rtcMin==51&&rtcSec<45) {
       angleWest = 0;
       angleEast = 118;
      return;
    }
       angleWest = 0;
       angleEast = 118;
  }
  if(rtcHour==17&&rtcMin>=51&&rtcMin<=26) {
    if(rtcMin==18&&rtcSec>=45){
       angleWest = 0;
       angleEast = 119;
      return;
    }
    else if(rtcMin==59&&rtcSec<=59) {
       angleWest = 0;
       angleEast = 119;
      return;
    }
      angleWest = 0;
      angleEast = 119;
  }
  
  if(rtcHour==18&&rtcMin<=9) {
      if(rtcMin==29&&rtcSec<=59) {
       angleWest = 0;
       angleEast = 120;
       return;
    }
       angleWest = 0;
       angleEast = 116;
  }

 /***********************************************************************
   * THIS IS TO CATCH EVERY OTHER TIME AND MAKE SURE IT'S ZERO FOR BOTH ANGLES
   ***********************************************************************/
  if((rtcHour<=4||rtcHour>=19)||(rtcHour==5&&rtcMin<=49)||(rtcHour==8&&rtcMin>=10)) {
       angleWest = 0;
       angleEast = 0;   
       return;
  }
  return;
}



